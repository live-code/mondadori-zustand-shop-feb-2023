import { useCartStore } from '../../core/store/cart.store';


export function CartPage() {
  return <div>
    <h1 className="pageTitle">Cart</h1>
    <CartList simplified={false}/>
  </div>
}


export interface CartListProps {
  simplified: boolean
}

export function CartList(props: CartListProps) {

  const list = useCartStore(state => state.list)
  const removeFromCart = useCartStore(state => state.removeFromCart)
  const increaseQty = useCartStore(state => state.increaseQty)
  const decreaseQty = useCartStore(state => state.decreaseQty)

  function getTotal() {
    return list.reduce((acc, item) =>
      acc + (item.product.cost * item.qty) , 0)
  }

  return <div>

    <ul>
      {
        list.map(item => {
          return <li key={item.product.id} className="flex justify-between">
            <div className="w-24">
              {item.product.name}
            </div>
            {
              !props.simplified &&
                <div>
                  <button
                    className="btn"
                    onClick={() => decreaseQty(item.product.id)}>-
                  </button>
                  qty: {item.qty}
                  <button
                    className="btn"
                    onClick={() => increaseQty(item.product.id)}>+
                  </button>
                </div>
            }
            <div>
              € {item.product.cost * item.qty}    (€ {item.product.cost})
              <button
                className="btn"
                onClick={() => removeFromCart(item.product.id)}
              >Remove</button>
            </div>
          </li>
        })
      }
    </ul>

    <h1 className="pageTitle">TOTAL: {getTotal()}</h1>
  </div>
}
