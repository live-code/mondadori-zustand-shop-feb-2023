import clsx from 'clsx';
import React from 'react';
import { Product } from '../../../model/product';

interface CMSProductsListProps {
  active: Partial<Product> | null;
  items: Product[];
  onDelete: (id: number) => void;
  onEditItem: (p: Partial<Product>) => void;
}

export function CMSProductsList(props: CMSProductsListProps) {
  return (
    <div>
      {
        props.items.map(p => {
          return (
            <div
              key={p.id}
              className={clsx(
                'flex justify-between my-2 ',
                { 'bg-sky-200': p.id === props.active?.id}
              )}
            >
              <div>{p.name}</div>

              <div className="flex gap-3 items-center">
                <div>€ { p.cost}</div>
                <button className="btn"
                        onClick={() => props.onDelete(p.id)}>delete</button>

                <button className="btn"
                        onClick={() => props.onEditItem(p)}>edit</button>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}
