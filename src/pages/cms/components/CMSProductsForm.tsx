import React, { useEffect, useState } from 'react';
import { useProductsStore } from '../../../core/store/products.store';
import { Product } from '../../../model/product';

interface CMSProductsFormProps {
  active: Partial<Product> | null;
  onEdit: (product: Partial<Product>) => void
  onAdd: (product: Partial<Product>) => void
  onClear: () => void
}

const initialState = { name: '', cost: 0};
export function CMSProductsForm(props: CMSProductsFormProps) {
  const [formData, setFormData] = useState<Partial<Product>>(initialState)

  useEffect(() => {
    if (props.active?.id) {
      setFormData(props.active);
    } else {
      setFormData(initialState)
    }
  }, [props.active])

  function changeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const value = e.currentTarget.value;
    const name = e.currentTarget.name;
    setFormData(prev => ({ ...prev, [name]: value}))
  }

  function saveHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (formData.id) {
      props.onEdit(formData)
    } else {
      props.onAdd(formData)
    }
  }

  return (
    <form onSubmit={saveHandler}>
      <pre>{JSON.stringify(props.active)}</pre>
      <input
        type="text" name="name"
        placeholder="Product name"
        value={formData.name}
        onChange={changeHandler}
      />
      <input
        type="text" name="cost"
        placeholder="Cost"
        value={formData.cost}
        onChange={changeHandler}
      />
      <button className="btn" type="submit">
        {formData.id ? 'EDIT' : 'ADD'}
      </button>

      <button type="button" onClick={props.onClear}>CLEAN</button>
    </form>
  )
}
