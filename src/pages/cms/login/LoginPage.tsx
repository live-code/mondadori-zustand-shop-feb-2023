import axios from 'axios';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import { useAuth } from '../../../core/auth/useAuth';
import { Auth } from '../../../model/auth';

export function LoginPage() {
  const [formData, setFormData] = useState({ username: '', password: ''})
  const login = useAuth(state => state.login)
  const isLogged = useAuth(state => state.isLogged)
  const navigate = useNavigate();

  /*useEffect(() => {
    console.log(isLogged)
    if (isLogged) {
      // navigate('/cms')
    }
  }, [isLogged])*/

  function changeHandler(e: ChangeEvent<HTMLInputElement>) {
    const value = e.currentTarget.value;
    const name = e.currentTarget.name;
    setFormData(s => ({ ...s, [name]: value}))
  }

  function loginHandler(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    login(formData.username, formData.password)
    // to fix
    setTimeout(() => {
      navigate('/cms')
    }, 1000)
  }

  return <div>

    <h1 className="title">LOGIN</h1>

    <form onSubmit={loginHandler}>
      <input type="text" placeholder="username" name="username" value={formData.username} onChange={changeHandler} />
      <input type="tet" placeholder="password" name="password" value={formData.password} onChange={changeHandler} />
      <button className="btn" type="submit">LOGIN</button>
    </form>

  </div>
}
