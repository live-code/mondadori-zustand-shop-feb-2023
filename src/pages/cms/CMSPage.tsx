import React, { useEffect, useState } from 'react';
import { useProductsStore } from '../../core/store/products.store';
import { PageTitle } from '../../shared/PageTitle';
import { CMSProductsForm } from './components/CMSProductsForm';
import { CMSProductsList } from './components/CMSProductsList';

export function CMSPage() {
  const products = useProductsStore(state => state.products)
  const error = useProductsStore(state => state.error)
  const pending = useProductsStore(state => state.pending)
  const activeItem = useProductsStore(state => state.activeItem)
  const loadProducts = useProductsStore(state => state.loadProducts)
  const addProduct = useProductsStore(state => state.addProduct)
  const editProduct = useProductsStore(state => state.editProduct)
  const deleteProduct = useProductsStore(state => state.deleteProduct)
  const setActiveItem = useProductsStore(state => state.setActiveItem)
  const clearActive = useProductsStore(state => state.clearActive)

  useEffect(() => {
    loadProducts();
  }, [])


  return <div>
    {error && <div>ahia!</div>}
    {pending && <div>loaders...</div>}

    <PageTitle title="CMS" />

    <CMSProductsForm
      active={activeItem}
      onAdd={addProduct}
      onEdit={editProduct}
      onClear={clearActive}
    />

    <hr className="my-4"/>

    <CMSProductsList
      active={activeItem}
      items={products}
      onEditItem={setActiveItem}
      onDelete={deleteProduct}
    />

  </div>
}
