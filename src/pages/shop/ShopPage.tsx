import { useEffect } from 'react';
import { useCartStore } from '../../core/store/cart.store';
import { useProductsStore } from '../../core/store/products.store';

export function ShopPage() {

  const loadProducts = useProductsStore(state => state.loadProducts)
  const products = useProductsStore(state => state.products)
  const addToCart = useCartStore(state => state.addToCart)
  const cartList = useCartStore(state => state.list)

  useEffect(() => {
    loadProducts()
  }, [])

  return <div>
    <div className="flex flex-wrap">
      {
        products.map(u =>
          <div key={u.id} className=" w-1/3 text-center mb-6 border-2 rounded-2xl ">
            <div>{u.name}</div>
            <div>€ {u.cost}</div>
            <button onClick={() => addToCart(u)} className="btn" >
              Add to cart</button>
          </div>
        )
      }
    </div>

    <pre>{JSON.stringify(cartList, null, 2)}</pre>
  </div>
}
