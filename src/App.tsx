import axios from 'axios';
import { useState } from 'react'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import './App.css'
import { Interceptor } from './core/auth/Interceptor';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { NavBar } from './core/NavBar';
import { CartPage } from './pages/cart/CartPage';
import { CMSPage } from './pages/cms/CMSPage';
import { LoginPage } from './pages/cms/login/LoginPage';
import { ShopPage } from './pages/shop/ShopPage';
import { UiKitPage } from './pages/uikit/UiKitPage';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Interceptor />
      <hr/>
      <div className="m-3">
        <Routes>
          <Route path="login" element={ <LoginPage /> } />
          <Route path="shop" element={ <ShopPage /> } />
          <Route path="cms" element={ <PrivateRoute role="admin"><CMSPage /></PrivateRoute> } />
          <Route path="cart" element={ <CartPage /> } />
          <Route path="uikit" element={ <UiKitPage /> } />
          <Route path="/" element={ <Navigate to="/shop" />} />
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App

