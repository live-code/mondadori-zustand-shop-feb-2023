import axios from 'axios';
import { create } from 'zustand';
import { Auth } from '../../model/auth';

interface AuthState {
  auth: Auth | null;
  isLogged: boolean;
  login: (u: string, p: string) => void;
  logout: () => void;
}

export const useAuth = create<AuthState>((set, get) => ({
  auth: null,
  isLogged: !!localStorage.getItem('auth'),
  login: (u: string, p: string) => {
    axios.get<Auth>(`http://localhost:3000/login?username=${u}&password=${p}`)
      .then(res => {
        set({ auth: res.data, isLogged: true})
        // localStorage.setItem('token', res.data.token)
        localStorage.setItem('auth', JSON.stringify(res.data))
      })
  },
  logout: () => {
    localStorage.removeItem('auth')
    set({ auth: null, isLogged: false})
  },
  token: () => {
    const authFromLocalStorage = localStorage.getItem('auth')
    if (authFromLocalStorage) {
      const auth: Auth = JSON.parse(authFromLocalStorage)
      return auth.token;
    }
    return null
  }
}))
