import axios from 'axios';
import { useEffect, useState } from 'react';
import { useInterceptor } from './useInterceptor';

export function Interceptor() {
  const { error } = useInterceptor();

  return <div>
    {error && <div>errore lato server</div>}
  </div>
}
