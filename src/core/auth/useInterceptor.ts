import axios from 'axios';
import { useEffect, useState } from 'react';
import { useAuth } from './useAuth';

export function useInterceptor() {
  const [error, setError] = useState<boolean>(false);
  //const token = useAuth(state => state.auth?.token)

  useEffect(() => {
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      setError(false)
      const tk = useAuth.getState().auth?.token;
      if (tk) {
        config.headers.set('Authorization', 'Bearer ' + useAuth.getState().auth?.token)
      }
      return config;
    }, function (error) {
      return Promise.reject(error);
    });

    // Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (error) {
      setError(true);
      return Promise.reject(error);
    });

  }, [])

  return {
    error,
  }

}
