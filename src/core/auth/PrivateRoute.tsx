import { PropsWithChildren } from 'react';
import { Navigate } from 'react-router-dom';
import { useAuth } from './useAuth';

interface PrivateRoutePRops {
  role: 'admin' | 'guest'
}
export function PrivateRoute(props: PropsWithChildren<PrivateRoutePRops>) {
  const isLogged = useAuth(state => state.isLogged)
  const currentRole = useAuth(state => state.auth?.role)

  return <>
    {
      isLogged && currentRole === props.role ?
        props.children :
        <Navigate to="/login" />
    }
  </>
}
