import { PropsWithChildren } from 'react';
import { useAuth } from './useAuth';

export function IfLogged(props: PropsWithChildren) {
  const isLogged = useAuth(state => state.isLogged)
  return <>
    {
      isLogged ?
        props.children :
        null
    }
  </>
}
