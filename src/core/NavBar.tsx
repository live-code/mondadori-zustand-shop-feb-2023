import { useEffect } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { IfLogged } from './auth/IfLogged';
import { useAuth } from './auth/useAuth';
import { CartPanelOverlay } from './CartPanelOverlay';
import { useCartStore } from './store/cart.store';


export function NavBar() {
  const cartList = useCartStore(state => state.list)
  const openCartPanel = useCartStore(state => state.openCartPanel)
  const logout = useAuth(state => state.logout)

  const navigate = useNavigate();

  function logoutHandler() {
    logout()
    navigate('/login')
  }

  return (
    <div className="bg-slate-900 text-white p-3 flex justify-between">
      <div className="flex gap-3 items-center">
        <NavLink to="login" className="text-xl">LOGO</NavLink>
        <NavLink to="shop">shop</NavLink>
        <IfLogged>
          <NavLink to="cms">cms</NavLink>
        </IfLogged>

        <IfLogged>
          <button className="btn" onClick={logoutHandler}>logout</button>
        </IfLogged>

      </div>


      <button className="btn" onClick={openCartPanel} >
        Cart: {cartList.length}
      </button>

      <CartPanelOverlay />

    </div>
  )
}
