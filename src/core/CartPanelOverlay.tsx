import { useNavigate } from 'react-router-dom';
import { CartList, CartPage } from '../pages/cart/CartPage';
import { useCartStore } from './store/cart.store';

export function CartPanelOverlay() {
  const isOpenCartPanel = useCartStore(state => state.isOpenCartPanel)
  const closeCartPanel = useCartStore(state => state.closeCartPanel)
  const navigate = useNavigate();

  function gotoCartHandler() {
    closeCartPanel();
    navigate('/cart');
  }

  return (
    <>
      {
        isOpenCartPanel &&
        <div className="bg-slate-200 fixed right-3 top-3 w-96 p-3 text-black">
          <div>
            <CartList simplified={true} />
          </div>
          <button
            className="btn"
            onClick={gotoCartHandler}>Go to Cart</button>
        </div>
      }
    </>
  )
}
