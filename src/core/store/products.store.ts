import axios from 'axios';
import { create } from 'zustand';
import { Product } from '../../model/product';

type ProductsState = {
  products: Product[];
  pending: boolean;
  error: boolean;
  activeItem: Partial<Product>;
  loadProducts: () => void;
  deleteProduct: (id: number) => void;
  addProduct: (product: Partial<Product>) => void;
  editProduct: (product: Partial<Product>) => void;
  setActiveItem: (product: Partial<Product> ) => void;
  clearActive: () => void;
}

export const useProductsStore = create<ProductsState>((set, get) => ({
  products: [],
  pending: false,
  error: false,
  activeItem: {},
  setActiveItem: (p: Partial<Product>) => {
    set({ activeItem: p})
  },
  clearActive: () => {
    set({ activeItem: {}})
  },
  loadProducts: async () => {
    set({ pending: true })
    try {
      const res = await axios.get('http://localhost:3000/products')
      set({ products: res.data, error: false, pending: false })
    } catch (e) {
      set({ error: true, pending: false})
    }

  },
  deleteProduct: async (id: number) => {
    set({ pending: true, error: false })
    try {
      await axios.delete('http://localhost:3000/products/' + id)
      set({ products: get().products.filter(u => u.id !== id), pending: false})
    } catch(e) {
      set({error: true, pending: false})
    }
  },
  addProduct: async (product: Partial<Product>) => {
    set({ pending: true, error: false })
    try {
      const res = await axios.post<Product>('http://localhost:3000/products/', product)
      set({ products: [...get().products, res.data], pending: false, activeItem: {}})
    } catch(e) {
      set({error: true, pending: false})
    }
  },
  editProduct: async (product: Partial<Product>) => {
    set({ pending: true, error: false })
    try {
      const res = await axios.patch<Product>(`http://localhost:3000/products/${product.id}`, product)
      set({ products: get().products.map(p => {
        return p.id === product.id ? res.data : p
      })})
    } catch(e) {
      set({error: true, pending: false})
    }
  },
}))
