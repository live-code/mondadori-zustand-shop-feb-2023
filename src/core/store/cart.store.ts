import { create } from 'zustand';
import { CartItem } from '../../model/cart-item';
import { Product } from '../../model/product';

export interface CartState {
  list: CartItem[];
  isOpenCartPanel: boolean;
  addToCart: (product: Product) => void;
  removeFromCart: (id: number) => void;
  increaseQty: (id: number) => void;
  decreaseQty: (id: number) => void;
  openCartPanel: () => void;
  closeCartPanel: () => void;
}

export const useCartStore = create<CartState>((set, get) => ({
  list: [],
  isOpenCartPanel: false,
  openCartPanel: () => {
    set({ isOpenCartPanel: true })
  },
  closeCartPanel: () => {
    set({ isOpenCartPanel: false })
  },
  addToCart: (product: Product) => {
    const find = get().list.find(item => item.product.id ===  product.id)

    if (find) {
      // increase qty
      set({
        list: get().list.map(item => {
          return item.product.id === product.id ? {...item, qty: item.qty + 1} : item
        }),
        isOpenCartPanel: true
      })
    } else {
      // set qty = 1
      const productToAdd: CartItem = {
        product: product,
        qty: 1
      }
      set({ list: [...get().list, productToAdd], isOpenCartPanel: true})
    }

  },
  removeFromCart: (id: number) => {
    set({ list: get().list.filter(item => item.product.id !== id )})
  },
  increaseQty: (id: number) => {
    const find = get().list.find(item => item.product.id === id)
      set({
        list: get().list.map(item => {
          return item.product.id === find?.product.id ? { ...find, qty: ++item.qty } : item
        })
      })
  },
  decreaseQty: (id: number) => {
    const find = get().list.find(item => item.product.id === id)
    if (find && find.qty > 0) {
      set({
        list: get().list.map(item => {
          return item.product.id === find?.product.id ? { ...find, qty: --item.qty } : item
        })
      })
    }
    if (find?.qty === 1) {
      get().removeFromCart(id);
    }

  },

}))
