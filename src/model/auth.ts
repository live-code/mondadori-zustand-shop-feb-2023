export interface Auth {
  token: string;
  role: 'admin' | 'guest';
  displayName: string;
}
