import React from 'react';

export const PageTitle = ({ title }: { title: string }) =>
  <h1 className="pageTitle">{title}</h1>
